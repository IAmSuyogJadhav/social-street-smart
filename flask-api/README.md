# Clickbait detection API

This is the API written in Flask for serving the clickbait detection model.

## Usage

1. Clone this repository.

2. Install all the dependencies by running:

   ```bash
   pip3 install -r requirements.txt
   ```

3. Start the server by executing `flask run` inside this directory. Note where the server is running (usually it is [localhost:5000](http://localhost:5000/) 

4. Send a GET request to `localhost:5000/predict`, with a post title in the `text` header. You will be greeted back with a response with the predicted class (Clickbait/ Non Clickbait) and the probability value.

## Example Usage

```bash
$ curl http://localhost:5000/predict?text=zuckerberg%20was%20stealing%20this%20critical%20info%20from%20you,%20click%20to%20find%20out!
```

Output:

```bash
{"Class":"Clickbait","Probability":0.7415780366607733}
```