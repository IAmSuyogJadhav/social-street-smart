# Importing a list of stopwords
import nltk
nltk.download('stopwords')
from nltk.corpus import stopwords
import pickle
import re
STOPWORDS = list((stopwords.words('english')))

def text_prepare(text, keep_stops=True):
    """
    text: A string
    keep_stops: Boolean, whether to keep stopwords or not. Defaults to True.
    returns: Processed initial string with bad symbols removed
             and punctuation replaced with single space.
    """
    # Lower the input string (can be replaced with str.lower())
    text = text.lower()
    text = re.sub(r'[/(){}\[\]\\|@,;]', r' ', text)
    text = re.sub(r'[^0-9a-z #+_]', r'', text)

    word_list = text.split()
    if not keep_stops:
        text = ' '.join([i for i in word_list if i not in STOPWORDS])

    return text

vec = pickle.load(open('./app/files/vectorizer.dat', 'rb'))
model = pickle.load(open('./app/files/model1.dat', 'rb'))

mapping = {
0: 'Non-Clickbait',
1: 'Clickbait'
}

def predict(text):
    text = vec.transform([text_prepare(text)])
    pred = int(model.predict(text))
    pred_proba = model.predict_proba(text).squeeze()[pred]

    return mapping[pred], pred_proba
