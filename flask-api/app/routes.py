from app import app
from flask import jsonify, request
from app.predict import predict

@app.route('/predict', methods=['GET', 'POST'])
def _predict():
    text = request.args.get('text')
    resp = {
        'Class': None,
        'Probability': None,
        # 'ErrorCode':0,  # TODO
        # 'Error': None  #TODO
    }

    c, p = predict(text)
    resp['Class'] = c
    resp['Probability'] = p

    # TODO: Implement error handling
    return jsonify(resp)
