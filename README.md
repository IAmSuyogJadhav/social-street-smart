# Social Street Smart

## Clickbait Detector

### Model

> View the raw training notebook [here](Clickbait_Detector.ipynb).
>
> OR
>
> Launch the notebook directly in Colab: <a href="https://colab.research.google.com/drive/1lsxAQ7x-xcLvPToj-xYwzYQmez23wBXd" target="_parent"><img src="https://colab.research.google.com/assets/colab-badge.svg" alt="Open In Colab"/></a>

Trained a very basic initial Model. Only used the post titles to predict the nature of post in this one.

- **Preprocessing:** 
  - Kept stopwords and removed punctuation.
  - Used TF-IDF as a numerical representation for the post titles.
- **Algorithm:** Used simple logistic regression on the TF-IDF values. Currently getting ~87% on train set and ~84% on validation set. 

### API

- Serialized the trained model and TF-IDF vectorizer into a python pickle, and served the model over an API designed in Flask.
- The output is returned in JSON format with two attributes:
  - `Class`: The predicted class, one of *"Clickbait"* and *"Non-Clickbait"*.
  - `Probability`: Float between 0 and 1. The confidence of the prediction. Higher the better.
- The API is live at https://suyogjadhav.pythonanywhere.com/predict

#### Example Usage

```python
import requests
url = "https://suyogjadhav.pythonanywhere.com/predict"
params = {
    # Change the following line to test your own phrase
	"text": "Zuckerberg was stealing this info from you, Check it out!"
}
resp=requests.get(url=api_endpoint, params=params)
print(resp.json())
```

Output:

```pytho
{'Class': 'Clickbait', 'Probability': 0.7760726285124612}
```

## To-Do's

- **Model**:
  - Try out advanced representations: Word2Vec, GloVe, FastText and see which works the best.
  - Design and use a 2-layered LSTM model
- **API**:
  - Implement error handling
- **Chrome Extension**
  - Design the chrome extension that will automatically detect all the posts on a page and will send it over detection API, and mark the posts as Clickbait/Non-Clickbait based on the output of the API.